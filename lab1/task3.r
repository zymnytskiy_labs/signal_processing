require('tuneR')

energy <- function(signal) {
	return(sum(signal ^ 2))
}

rec <- readWave('rec.wav')
rec2 <- readWave('rec2.wav')

metrics = sqrt(energy(rec@left - rec2@left))
print(metrics)

