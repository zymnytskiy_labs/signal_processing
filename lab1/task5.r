x = seq(0, 10 * pi, 0.1)

simple_harmonic = sin(x)
poly_harmonic = sin(x) + sin(2 * x) + sin(3 * x) + sin(4 * x + pi)
aperiodic = runif(length(x))

plot(x, poly_harmonic, type='l', col='red')
lines(x, simple_harmonic, col='blue')
lines(x, aperiodic, col='green')

