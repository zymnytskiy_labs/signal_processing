require('EBImage')

original <- readImage('gray.png')
luminance <- channel(original, 'luminance') # it's a bad idea for grayscale images, this function returns relative luminance by RGB values

writeImage(luminance, 'luminance.png')

require('tuneR')

recorded <- readWave('rec.wav')

png(filename='rec.png')
plot(recorded)
dev.off()

