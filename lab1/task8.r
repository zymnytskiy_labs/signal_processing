require('EBImage')

red = readImage('original.png')
imageData(red)[,,2:3] = 0

green = readImage('original.png')
imageData(green)[,,1] = 0
imageData(green)[,,3] = 0

blue = readImage('original.png')
imageData(blue)[,,1:2] = 0

display(red)
display(green)
display(blue)

changed = readImage('original.png')
imageData(changed)[,,1] = imageData(blue)[,,3]
imageData(changed)[,,3] = imageData(red)[,,1]
display(changed)

random = readImage('original.png')
imageData(random)[,,2] = runif(length(imageData(red)[,,1]))
display(random)
