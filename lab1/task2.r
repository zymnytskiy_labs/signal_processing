require('tuneR')

energy <- function(signal) {
	return(sum(signal ^ 2))
}

rec <- readWave('rec.wav')

rec_energy = energy(rec@left)
print(rec_energy)

rec_norm = sqrt(rec_energy)
print(rec_norm)

rec_duration <- length(rec) / rec@samp.rate
rec_power = rec_energy / rec_duration
print(rec_power)

png(filename='rec_power.png')
plot(seq(1, length(rec)) / rec@samp.rate, rec@left ^ 2)
dev.off()

