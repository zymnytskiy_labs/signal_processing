require('EBImage')

width <- 256
height <- 256

data <- matrix(runif(width * height), width, height)
img = Image(data)

display(img)

