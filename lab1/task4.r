len <- 6 * 17
start <- -floor(len / 2)
end <- -start

x = seq(start, end, 0.1)

gaussian <- function(x, m, t) {
	(1 / t * sqrt(2 * pi)) * exp(-((x - m)^2) / 2 * t ^ 2)
}

error <- function(x) {
	sapply(x, function(x) { (2 / sqrt(pi)) * integrate(function(t) { exp(-t^2) }, 0, x)$value })
}

heaviside <- function(x) {
	sapply(x, function(x) { if (x >= 0) 1 else 0 })
}

plot(x, gaussian(x, 0, 0.1), type='l', col='red')
lines(x, error(x), col='blue')
lines(x, heaviside(x), col='green')

