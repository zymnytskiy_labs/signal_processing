build_freq <- function(data, num) {
	min_data = min(data)
	step <- (max(data) - min_data) / num
	x <- ceiling((data - min_data) / step)
	seq_x = seq(0, max(x))
	data.frame(value=seq_x*step + min_data, count=sapply(seq_x, function(v) { length(which(x == v)) }))
}

build_hist <- function(freq, line) {
	tmp <- tail(freq$count, -1)
	tmp[1] <- tmp[1] + 1
	bp <- barplot(tmp / sum(tmp))
	lines(x=bp, y=line)
	points(x=bp, y=line)
}

equalize <- function(img, freq) {
	N = length(img)
	data <- imageData(img)
	ff <- 1 / length(freq$value)

	find_freq <- function(val) {
		i <- floor(val / ff) + 1
		return(c(freq$value[i], freq$count[i], i))
	}

	newData = sapply(data, function(d) {
		f = find_freq(d)
		sum(freq$value[1:f[3]])
	})

	Image(newData, original@dim)
}

original = readImage('gray.png')
