impulseResponse <- function(x) {
	sapply(x, function(x) { if (x == 15) -0.5 else 0 })
}

inputSignal <- function(x) {
	cos(x) + cos(20 * x)
}

convolution <- function(input, impulseResponse) {
	N <- length(input)
	M <- length(impulseResponse)
	result = sapply(seq(1, N + M), function(i) { 0 });

	for (i in seq(1, N)) {
		for (j in seq(1, M)) {
			result[i + j] = result[i + j] + input[i] * impulseResponse[j]
		}
	}

	result
}

