require('EBImage')

Y1 <- readImage('gray.png')
imageData(Y1) = imageData(Y1) ** 0.2
writeImage(Y1, 'Y1.png')

Y2 <- readImage('gray.png')
imageData(Y2) = imageData(Y2) ** 0.02
writeImage(Y2, 'Y2.png')

Y3 <- readImage('gray.png')
imageData(Y3) = imageData(Y3) ** 3
writeImage(Y3, 'Y3.png')

Y4 <- readImage('gray.png')
imageData(Y4) = imageData(Y4) ** 17
writeImage(Y4, 'Y4.png')

