data1 <- unlist(read.table('Lb2_4_1.prn', sep='\n'), use.names = FALSE)
data2 <- unlist(read.table('Lb2_4_2.prn', sep='\n'), use.names = FALSE)

build_freq <- function(data, num) {
	min_data = min(data)
	step <- (max(data) - min_data) / num
	x <- ceiling((data - min_data) / step)
	seq_x = seq(0, max(x))
	data.frame(value=seq_x*step + min_data, count=sapply(seq_x, function(v) { length(which(x == v)) }))
}

build_hist <- function(freq, line) {
	tmp <- tail(freq$count, -1)
	tmp[1] <- tmp[1] + 1
	bp <- barplot(tmp / sum(tmp))
	lines(x=bp, y=line)
	points(x=bp, y=line)
}

# data1 - exponential distribution
# data2 - uniform distribution, I guess

freq1 <- build_freq(data1, 30)
freq2 <- build_freq(data2, 30)

lambda <- length(data1) / sum(data1)

a <- min(data2)
b <- max(data2)

Xa <- 43.77

P1 <- function(i) { exp(-lambda * freq1$value[i]) - exp(-lambda * freq1$value[i + 1]) }
P2 <- function(i) { ((b - a) / 30) / (b - a) }

v1 <- tail(freq1$count, -1)
v1[1] <- v1[1] + 1
X21 <- sum(sapply(seq(1, length(v1)), function(i) { (v1[i] - length(data1) * P1(i)) ^ 2 / (length(data1) * P1(i)) }))
v2 <- tail(freq2$count, -1)
v2[1] <- v2[1] + 1
X22 <- sum(sapply(seq(1, length(v2)), function(i) { (v2[i] - length(data2) * P2(i)) ^ 2 / (length(data2) * P2(i)) }))

X21_wrong <- sum(sapply(seq(1, length(v1)), function(i) { (v1[i] - length(data1) * P2(i)) ^ 2 / (length(data1) * P2(i)) }))
X22_wrong <- sum(sapply(seq(1, length(v2)), function(i) { (v2[i] - length(data2) * P1(i)) ^ 2 / (length(data2) * P1(i)) }))

# functions for hist building with P
# build_hist(freq1, P1(seq(1, length(freq1$value) - 1)))
# build_hist(freq2, rep(P2(seq(1, length(freq2$value) - 1)), length(freq2$value) - 1))

