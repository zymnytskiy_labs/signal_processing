gauss <- function(x, a, b, c) { a * exp(-((x - b) ^ 2) / (2 * c ^ 2)) }
func <- function(x) { gauss(x, 1, 0, 1) + cos(x) }
len <- function(c) { sqrt(Re(c) ^ 2 + Im(c) ^ 2) }

f <- function(k, values) {
	(1 / sqrt(length(values))) * sum(sapply(seq(0, length(values) - 1), function(n) { values[n+1] * exp(-1i * 2 * pi * n * k / length(values)) }))
}

y <- function(n, values) {
	(1 / sqrt(length(values))) * sum(sapply(seq(0, length(values) - 1), function(k) { values[k+1] * exp(1i -  2 * pi * n * k / length(values)) }))
}

ycos <- function(n, values) {
	N = length(values)
	(2 / sqrt(N)) * sum(sapply(seq(0, N - 1), function(k) { values[k + 1] * cos(2 * pi * n * k / N) }))
}

ysin <- function(n, values) {
	N = length(values)
	(2 / sqrt(N)) * sum(sapply(seq(0, N - 1), function(k) { values[k + 1] * sin(2 * pi * n * k / N) }))
}

data <- func(seq(-5, 5, 0.1))
xs = sapply(seq(0, length(data) - 1), function(k) { f(k, data); })
amplitudes = len(xs)
ys = sapply(seq(0, length(data) - 1), function(n) { y(n, xs); }) # doesn't work
ys = sapply(seq(0, length(data) - 1), function(n) { ycos(n, Re(xs)) + ysin(n, Im(xs)); })
fa = sapply(seq(0, length(data) - 1), function(k) {
		    y <- f(k, data)
		    atan(sin(Re(y)) / cos(Im(y)))
})

get_energy <- function(values, part) {
	energy <- sum(values ^ 2) * part

	for (n in seq(1, length(values))) {
		if (sum(values[seq(1, n)] ^ 2) * 2 >= energy) {
			return(n)
		}
	}

	stop("can't get energy")
}

